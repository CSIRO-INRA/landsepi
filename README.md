# {landsepi} : Landscape Epidemiology and Evolution <img src="man/figures/landsepi-logo.png" align="right" alt="" width="120" />

<!-- badges: start -->
[![pipeline status](https://gitlab.paca.inrae.fr/CSIRO-INRA/landsepi/badges/master/pipeline.svg)](https://gitlab.paca.inrae.fr/CSIRO-INRA/landsepi/commits/master)
[![CRAN status](https://www.r-pkg.org/badges/version/landsepi)](https://cran.r-project.org/package=landsepi)
<!-- badges: end -->

## Description
A stochastic, spatially-explicit, demo-genetic model simulating the spread and evolution 
of a plant pathogen in a heterogeneous landscape to assess resistance deployment strategies. 
It is based on a spatial geometry for describing the landscape and allocation of 
different cultivars, a dispersal kernel for the dissemination of the pathogen, and a SEIR 
('Susceptible-Exposed-Infectious-Removed’) structure with a discrete time step.
It provides a useful tool to assess the performance of a wide range of deployment options with 
respect to their epidemiological, evolutionary and economic outcomes.  


## Authors and contributors

* Loup Rimbaud. Author.  
* Marta Zaffaroni. Author.  
* Julien Papaix. Author.  
* Jean-François Rey. Author, maintainer.   
* Jean-Loup Gaussen. Contributor.  
* Manon Couty. Contributor.  


## Funding

This work benefited from :  

* ANR project "ArchiV" (2019–2023, grant n°ANR-18-CE32-0004-01),  
* AFB Ecophyto II-Leviers Territoriaux Project ”Médée” (2020–2023),  
* GRDC grant CSP00192 and the CSIRO/INRA linkage program,  
* ANR project 'COMBINE' (2022-2026, grant n°ANR-22-CE32-0004),  
* SPE project 'DYNAMO'(2022-2024),  
* INRA Program ASC (2008-2014), 


## Citation

Rimbaud L, Zaffaroni M, Rey J, Papaïx J (2024). landsepi: Landscape Epidemiology and Evolution. R package version 1.4.0, https://cran.r-project.org/package=landsepi. 


## License

The package landsepi is under GNU GPL V2.  
See LICENSE file.


## Installation

### Configuration and dependencies
Read file "DESCRIPTION" for details on configuration requirements (with linux and R).  
The package for compiling needs R (version >= 4.2.0) as well as g++, gsl dev library and ffmpeg (for videos).  

Under Linux OS:  
```bash
sudo apt-get install g++ libgsl2 libgsl-dev ffmpeg
```

Under Windows use the zip.  
> You can install Rtools and get g++, gsl and ffmpeg binaries under Windows (using pacman).

R packages dependencies :  
* Rcpp 
* sp
* Matrix (>= 1.3-0)
* mvtnorm
* fields
* splancs
* sf  
* DBI
* RSQLite
* foreach
* doParallel
* deSolve


To install :  
```r
install.packages(c("Rcpp","sp","Matrix","mvtnorm","fields","splancs","sf","DBI","RSQLite","foreach","doParallel","deSolve"))
```

### Install landsepi

To install the last version (from artifacts) : 

Unzip the artifacts.zip archive to get the package.  
And then run in a terminal (or using Rstudio interface)
```bash
# linux
R CMD INSTALL landsepi_*.*.*.tar.gz
# windows
R CMD INSTALL landsepi_*.*.*.zip
```

## Development

### Generate documentation before build and install
Open R :  

```r
library(Rcpp)
library(roxygen2)
Rcpp::compileAttributes(pkg=".")
roxygen2::roxygenize('.', roclets=c('rd', 'namespace'))
```

### Build, check and install the package

```bash
R CMD build . --resave-data
R CMD check --as-cran landsepi_*.*.*.tar.gz
R CMD INSTALL landsepi_*.*.*tar.gz
```

For debug mode : 
```
R CMD INSTALL landsepi_*.*.*.tar.gz --with-keep.source --configure-args="--enable-debug=yes"
```

## Package users: Documentation and demonstration

> Take a look at the [vignettes/](vignettes/)

A complete description of the package and its functions is provided in the R documentation.
Open R:  
```r
library(landsepi)
??landsepi   
## select <landsepi::landsepi-package> for a complete description of the package

## Also access the vignettes (tutorials) via:
browseVignettes("landsepi")
```

Run demonstrations (in 20-year simulations) for different deployment strategies:
```r
demo_landsepi(strat = "MO") ## for a mosaic of cultivars
demo_landsepi(strat = "MI") ## for a mixture of cultivars
demo_landsepi(strat = "RO") ## for a rotation of cultivars
demo_landsepi(strat = "PY") ## for a pyramid of resistance genes
```

## Shiny interface

You can try the [online Landsepi R Shiny App](https://shiny.biosp.inrae.fr/app/landsepi)  
or run it into R. To run the shiny interface into R you will need the following packages:
```r
Install packages : install.packages(c("shiny","DT", "shinyjs", "gridExtra", "png", "grid"
, "future", "promises", "tools", "shinyalert"))
```

The app can be launched with:
```r
library(landsepi)
runShinyApp()
```

## Get started

To get started see the [first article](articles/O1-run_simple_simul.html)  

## Future versions

Future versions of the package will include in particular:  

- Sets of pathogen parameters to simulate other pathosystems 
(e.g. cucumber mosaic virus [CMV] or potato virus Y [PVY] on pepper).  
- An updated version of the shiny app.  
- Pathogen adaptation to chemical treatments.  




