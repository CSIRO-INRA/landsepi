#' @title Package test strategies no pathogen
#' @name test_strategies_nopatho
#' @description Run strategies.
#' @param seed an integer used as seed value (for random number generator)
#' @param Nyears number of year to simulate
#' @details Run a simulation without pathogen
#'   The generated model outputs are text files, graphics and a video.
#' @include RcppExports.R AgriLand.R graphLand.R multiN.R periodic_cov.R
#' @importFrom utils data
#' @export
test_strategies_nopatho <- function(seed = 12345, Nyears = 3) {
  message("Running Strategy without pathogen")

  ## Simulation, pathogen, landscape and dispersal parameters
  simul_params <- createSimulParams(outputDir = getwd())
  simul_params <- setSeed(simul_params, seed)
  simul_params <- setTime(simul_params, Nyears, nTSpY=120)
  simul_params <- setLandscape(simul_params, loadLandscape(1))

  ## Cultivars
  cultivar <- loadCultivar(name = "Susceptible", type = "wheat")
  simul_params <- setCultivars(simul_params, cultivar)

  ## Croptypes
  croptype <- data.frame(croptypeID = c(0), croptypeName = c("No pathogen"), Susceptible = c(1.0))
  simul_params <- setCroptypes(simul_params, croptype)

  simul_params <- allocateLandscapeCroptypes(simul_params,
    rotation_period = 0,
    rotation_sequence = list(c(0)),
    rotation_realloc = FALSE,
    prop = 1,
    aggreg = 1
  )
  
  ## Pathogen and inoculum
  simul_params <- setPathogen(simul_params, loadPathogen("no pathogen"))
  simul_params <-setInoculum(simul_params, 0)
  
  ## list of outputs to be generated
  outputlist <- loadOutputs()
  simul_params <- setOutputs(simul_params, outputlist)
  
  ## Check, save and run simulation
  checkSimulParams(simul_params)
  simul_params <- saveDeploymentStrategy(simul_params, overwrite = TRUE)
  runSimul(simul_params, graphic = TRUE, videoMP4 = FALSE)
}
