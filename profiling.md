# Profiling C++ code

## Dependencies

Rtools
install google-perftools / gproftools

https://gperftools.github.io/gperftools/cpuprofile.html
https://github.com/gperftools/gperftools/wiki

## Install

https://github.com/gperftools/gperftools


### Linux
```
apt-get install libgoogle-perftools-dev libgoogle-perftools
```

Rerun documentation (roxygen) and Rcpp::CompileAttributs().  


```
R CMD INSTALL --no-multiarch --with-keep.source -d --configure-args="--enable-debug --enable-prof"
```


### windows

Not possible yet...

```
pacman -S gperftools
```

In src/Makevars.win :
* add _-lprofiler_ at PKG\_LIBS or PKG\_LDFLAGS
* add -DDEBUG=1 at PKG\_CPPFLAGS

Rerun documentation (roxygen) and Rcpp::CompileAttributs().  

In terminal  
```
R CMD INSTALL --no-multiarch --with-keep.source  -d --configure-args="--enable-debug --enable-prof"
```

In Rstudio -> Build -> configure Build Tools -> Install and Restart field  
`--configure-args="--enable-debug --enable-prof"`


## Use prof

In R :  
```
start_profiler("/tmp/profile.out")
demo_landsepi()
stop_profiler()
```

In terminal:  
```
google-pprof --pdf /home/jfrey/R/x86_64-pc-linux-gnu-library/4.0/landsepi/libs/landsepi.so /tmp/prof.out > landsepiProf.pdf
```

> googple-pprof can be named differently : pprof, gpprof...
> Change .so or .dll path

## Options

* __CPUPROFILE\_FREQUENCY__=x	default: 100	How many interrupts/second the cpu-profiler samples
* __LD\_PRELOAD__=/usr/local/lib/libprofiler.so
* __CPUPROFILE__=prof.out

